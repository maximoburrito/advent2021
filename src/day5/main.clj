(ns day5.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;; parsing
(defn parse-int [nstr]
  (Integer/parseInt nstr))

(defn parse-point [pstr]
  (let [[xstr ystr] (str/split pstr #",")]
    [(parse-int xstr) (parse-int ystr)]))

(defn parse-line [line]
  (let [[p1str p2str] (str/split line #" -> ")]
    [(parse-point p1str) (parse-point p2str)]))

(defn read-input [file]
  (map parse-line (str/split-lines (slurp (io/resource file)))))


(def test-input (read-input "day5/test.txt"))
(def full-input (read-input "day5/input.txt"))

;;; part1

(defn prange [i j]
  (if (<= i j)
    (range i (inc j))
    (range j (inc i))))


(defn hv-points [line]
  (let [[[x1 y1] [x2 y2]] line]
    (cond
      (= x1 x2)
      (for [y (prange y1 y2)]
        [x1 y])

      (= y1 y2)
      (for [x (prange x1 x2)]
        [x y2]))))


(defn record-point [points p]
  (if (points p)
    (update points p inc)
    (assoc points p 1)))

(defn count-covered [points-map]
  (count
   (for [[p n] points-map
         :when (> n 1)]
     p)))

(defn print-points [points-map size]
  (for [y (range 0 size)]
    (for [x (range 0 size)]
      (or (points-map [x y] 0)))))

(defn part1 [input]
  (loop [lines input
         points {}]
    (if (empty? lines)
      (count-covered points)
      (let [covered (hv-points (first lines))]
        (recur (rest lines)
               (reduce record-point points covered))))))

;;; part2

(defn diagonal-range [x1 y1 x2 y2]
  (if (> x1 x2)
    (diagonal-range x2 y2 x1 y1)
    (for [n (range (inc (- x2 x1)))]
      [(+ x1 n)
       (if (> y1 y2)
         (- y1 n)
         (+ y1 n))])))

(defn hvd-points [line]
  (let [[[x1 y1] [x2 y2]] line]
    (cond
      (= x1 x2)
      (for [y (prange y1 y2)]
        [x1 y])

      (= y1 y2)
      (for [x (prange x1 x2)]
        [x y2])

      true ; assume perfect diagonal
      (diagonal-range x1 y1 x2 y2))))

(defn part2 [input]
  (loop [lines input
         points {}]
    (if (empty? lines)
      (count-covered points)
      (let [covered (hvd-points (first lines))]
        (recur (rest lines)
               (reduce record-point points covered))))))


(comment
  (part1 test-input)
  5
  (part1 full-input)
  5585

  (part2 test-input)
  12
  (part2 full-input)
  17193)
