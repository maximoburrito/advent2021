(ns day18.main
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]
            [clojure.string :as str]
            [clojure.zip :as zip]))

;; I've never actually used zippers before. I had no idea
;; what I was doing when I started. I'm not sure I do now either,
;; but I did ultimately get something working. Ugly, but done!


;;; ---------- input

(defn parse-input [text]
  (mapv edn/read-string
    (str/split-lines text)))

(def test-input (parse-input (slurp (io/resource "day18/test.txt"))))
(def full-input (parse-input (slurp (io/resource "day18/input.txt"))))

;;; ---------- part1

(defn find-explodable [zexpr depth]
  (cond
    (not (zip/branch? zexpr))
    nil

    (= depth 4)
    zexpr

    :else
    (or
      (find-explodable (-> zexpr zip/down) (inc depth))
      (find-explodable (-> zexpr zip/down zip/right) (inc depth)))))


(defn until-leaf [zexpr zip-direction]
  (cond
    (or (not zexpr) (zip/end? zexpr))
    nil

    (zip/branch? zexpr)
    (until-leaf (zip-direction zexpr) zip-direction)

    :else
    zexpr))

(defn edit-left [zexpr edit-fn]
  (let [left (until-leaf (zip/prev zexpr) zip/prev)]
    (if left
      (let [replaced (zip/edit left edit-fn)]
        (until-leaf (zip/next replaced) zip/next))
      zexpr)))

(defn edit-right [zexpr edit-fn]
  (let [right (until-leaf (zip/next zexpr) zip/next)]
    (if right
      (let [replaced (zip/edit right edit-fn)]
        (until-leaf (zip/prev replaced) zip/prev))
      zexpr)))

(defn edit-plus [expr n]
  (if (number? expr)
      (+ expr n)
      [expr n]))

(defn do-explode [explodeme]
  (let [[l r] (zip/children explodeme)]
    (-> explodeme
      (zip/replace 0)
      (edit-left #(edit-plus % l))
      (edit-right #(edit-plus % r))
      (zip/root)
      (zip/vector-zip))))


(defn find-split [zexpr]
  (cond
    (zip/branch? zexpr)
    (or
      (find-split (-> zexpr zip/down))
      (find-split (-> zexpr zip/down zip/right)))

    (>= (zip/node zexpr) 10)
    zexpr

    :else
    nil))

(defn split [n]
  (let [half (quot n 2)]
    [half (- n half)]))

(defn reduce-expr [expr]
  (loop [zexpr (zip/vector-zip expr)]
    (if-let [explodeme (find-explodable zexpr 0)]
      (recur (do-explode explodeme))
      (if-let [split-me (find-split zexpr)]
        (recur (zip/vector-zip (zip/root (zip/replace split-me (split (zip/node split-me))))))
        (zip/root zexpr)))))

(defn magnitude [expr]
  (if (number? expr)
    expr
    (+
      (* 3 (magnitude (first expr)))
      (* 2 (magnitude (second expr))))))

(defn part1 [input]
  (magnitude (reduce #(reduce-expr [%1 %2]) input)))


;;; ---------- part2


(defn part2 [input]
  (reduce max
    (for [n1 input
          n2 input
          :when (not= n1 n2)]
      (magnitude (reduce-expr [n1 n2])))))


;;; ----------

(comment
  (part1 test-input)
  4140
  (part1 full-input)
  4391

  (part2 test-input)
  3993
  (part2 full-input)
  4626)
