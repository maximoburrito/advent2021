(ns day15.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;; ---------- input
(defn parse-input [text]
  (reduce merge {}
    (let [lines (str/split-lines text)
          size (count lines)]
      (for [y (range size)
            x (range size)
            :let [c (nth (nth lines y) x)
                  n (- (int c) (int \0))]]
        {[x y] n}))))

(defn read-input [which]
  (slurp (io/resource (format "day15/%s.txt" which))))

(def test-input (parse-input (read-input "test")))
(def full-input (parse-input (read-input "input")))

;; part1
(defn neighbors [[x y]]
  [[(dec x) y]
   [(inc x) y]
   [x (dec y)]
   [x (inc y)]])

(defn all-next [cave known-paths]
  (for [p (keys known-paths)
        n (neighbors p)
        :when (cave n)
        :when (not (known-paths n))]
    [n (+ (known-paths p)
         (cave n))]))

(defn part1 [input]
  (let [start [0 0]
        goal (last (sort (keys input)))]

    ;; track the known best paths
    ;; each iteration, I compute the scores of all the points
    ;; I can reach that I don't know the best score for. From those,
    ;; I add the lowest score to the known paths. This is very slow
    ;; because I'm constantly recomputing things I've computed before
    (loop [known {start 0}]
      (if (known goal)
        (known goal)
        (let [[next cost] (apply min-key second (all-next input known))]
          (recur (assoc known next cost)))))))

;;; ---------- part2

(defn inc-risk [n]
  (if (= n 9) 1 (inc n)))

(defn bigger [n offset]
  (nth (iterate inc-risk n) offset))

;; expand the board size by 5 in both dimensions
;; a little rough, but hey...
(defn expand-input [input]
  (let [size (inc (first (last(sort (keys input)))))]
    (reduce merge
      (for [x (range size)
            y (range size)
            x' (range 5)
            y' (range 5)]
        {[(+ x (* x' size))
          (+ y (* y' size))]
         (bigger
           (input [x y])
           (+ x' y'))}))))

(defn part2 [input]
  (let [cave (expand-input input)
        start [0 0]
        goal (last (sort (keys cave)))]

    ;; known - nodes we know the best score for
    ;; frontier - nodes we know a possible score for
    (loop [known {}
           frontier {start 0}]

      (if (known goal)
        ;; if we know the best score for the goal, then done
        (known goal)

        ;; otherwise, we take the lowest score path from our
        ;; frontier (next) and move it to the known paths
        ;; then we adjust the frontier scores for all of the neighbors
        ;; in case we've discovered a new lower path
        (let [next (apply min-key frontier (keys frontier))
              cost (frontier next)

              neighbor-costs
              (reduce merge
                (for [n (neighbors next)
                      :when (cave n)
                      :when (not (known n))]
                  {n (+ cost (cave n))}))]


          (recur (assoc known next cost)
            (dissoc (merge-with min frontier neighbor-costs) next)))))))

;; ---------- results
(comment
  (part1 test-input)
  40
  (part1 full-input)
  456
  (part2 test-input)
  315
  (part2 full-input)
  2831)
