(ns day24.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))


;; ---------- parsing
(defn parse-int [nstr]
  (Integer/parseInt nstr))

(defn parse-arg [word]
  (cond
    (nil? word) nil
    (re-matches #"[a-z]" word) (keyword word)
    :else (parse-int word)))

(defn parse-line [line]
  (let [[op arg1 arg2] (str/split line #" ")]
    {:op (keyword op)
     :arg1 (parse-arg arg1)
     :arg2 (parse-arg arg2)}))

(defn parse-text [text]
  (mapv parse-line (str/split-lines text)))

(def test-input (parse-text (slurp (io/resource "day24/test.txt"))))
(def full-input (parse-text (slurp (io/resource "day24/input.txt"))))

;; ----------

;; base26 math
;; (= (explode26 1000) [1 12 12])
;; (= (combine26 [1 12 12]) 1000)

(defn combine26 [ns]
  (reduce
    #(+ (* %1 26) %2)
    0
    ns))

(defn explode26 [n]
  (if (zero? n)
    []
    (concat
      (explode26 (quot n 26))
      [(mod n 26)])))


(defn mval [mem arg]
  (or (get mem arg) arg))


;; goal constants were determined by analysis. I used some code at the repl to gather this
;;
(def goals [[13 10]  ;; + PUSH 13+w
            [10 13]  ;; + PUSH 10+1
            [3 13]   ;; + PUSH 3+w
            [1 -11]  ;; - POP when stack-11 = w
            [9 11]   ;; + PUSH 9+w
            [3 -4]   ;; - POP when stack-4 = w
            [5 12]   ;; + PUSH 5+w
            [1 12]   ;; + PUSH 1+w
            [0 15]   ;; + PUSH 0+w
            [13 -2]  ;; - POP when stack-2=w
            [7 -5]   ;; - POP when stack-5=w
            [15 -11] ;; - POP when stack-11=w
            [12 -13] ;; - POP when stack-13=w
            [8 -10]  ;; - POP when stack-10=w
            ])

;; this runs the code with some input, I add the goal tracking so the machine
;; could prompt about expected values for debgging
(defn eval-code
  ([code input-nums]
   (eval-code code
     {:goals goals}
     input-nums))

  ([code initial-mem input-nums]
   (loop [input input-nums
          pc 0
          mem (merge {:w 0 :x 0 :y 0 :z 0} initial-mem)]

     (if (>= pc (count code))
       ;; all done, add the stack for debugginh
       (assoc mem :stack (explode26 (:z mem)))

       (let [{:keys [op arg1 arg2] :as line} (nth code pc)]
         (cond

           ;; added debug coditional when arg1=arg1, print the memory
           (= :debug op)
           (do
             (when (=
                     (mval mem arg1)
                     (mval mem arg2))
               (println "  | " mem))
             (recur input
               (inc pc)
               mem))

           ;; this is cluttered due to debugging
           ;; added logic so that if the input is empty it will  jump to the end of input
           ;; this helps with repl probing
           (= :inp op)
           (if (seq input)
             (do
               (let [stack (into [] (explode26 (:z mem)))
                     [y' x'] (first (:goals mem))]
                 (println (format "%s - read [%s]" stack (first input)))
                 (if (pos? x')
                   (println (format " - push %d+i(%d) = %d" y' (first input) (+ y' (first input))))
                   (do
                     (println (format " - compare z(%d)+%d to i(%d)"
                                (mod (:z mem) 26)
                                x'
                                (first input)))
                     (when (not= (+ (mod (:z mem) 26) x')
                             (first input))
                       (println (format " - fail=shift and push %d+i(%d)" y' (first input)))))))

               (recur
                 (rest input)
                 (inc pc)
                 (-> mem
                   (assoc arg1 (first input))
                   (update :goals rest))))
             ;; halt for debugging
             (recur (rest input) (count code)
               (-> mem
                 (assoc :halt pc)
                 (assoc :goals nil))))

           (= :mul op)
           (recur
             input
             (inc pc)
             (assoc mem arg1 (* (mval mem arg1) (mval mem arg2))))

           (= :add op)
           (recur
             input
             (inc pc)
             (assoc mem arg1 (+ (mval mem arg1) (mval mem arg2))))

           (= :mod op)
           (recur
             input
             (inc pc)
             (assoc mem arg1 (mod (mval mem arg1) (mval mem arg2))))

           (= :div op)
           (recur
             input
             (inc pc)
             (assoc mem arg1 (quot (mval mem arg1) (mval mem arg2))))

           (= :eql op)
           (recur
             input
             (inc pc)
             (assoc mem arg1 (if (= (mval mem arg1) (mval mem arg2))
                               1 0)))

           :else
           [:??? pc line mem]))))))



;; ----------
;; this is a random checker I used from the repl to compare as a modified
;; a test input file and the real input file as I modified the test input
;; to gain understanding of the execution. If the change I made doesn't produce
;; the same output (for some random input, print something out)

(defn rand-in []
  (take 14 (repeatedly #(inc (rand-int 9)))))

(defn check [in]
  (let [a (eval-code (parse-text (slurp (io/resource "day24/test.txt")))
            in)
        b (eval-code (parse-text (slurp (io/resource "day24/input.txt")))
             in)]
    [in (if (= a b) :ok [a b])]))

(defn tests []
  (while true
    (Thread/sleep 500)
    (println (check (rand-in)))))


;; ----------

(defn input-blocks [input n]
  (let [block-size 18]
    (->> input
      (drop (* block-size (dec n))))))

;; run the last N inputs, assumes 18 long sections
(defn run-last [stack input]
  (eval-code (input-blocks full-input (- 15 (count input)))
    {:z (combine26 stack)
     :goals (take-last (count input) goals)}
    input))


(comment
  ;; this is a log (from bottom to top) as I ran the final
  ;; instructions on inputs, growing it by hand from the repl
  ;; as I begin to understand the structure of the valid inptus

  ;; + PUSH 13+w       ; +3
  ;; +  PUSH 10+1      ; -3
  ;; +   PUSH 3+w      ; -8
  ;; -   POP when stack-11 = w
  ;; +   PUSH 9+w      ; +5
  ;; -   POP when stack-4 = w
  ;; +   PUSH 5+w     ; -6
  ;; +    PUSH 1+w    ; -4
  ;; +     PUSH 0+w   ; -2
  ;; -     POP when stack-2=w
  ;; -    POP when stack-5=w
  ;; -   POP when stack-11=w
  ;; -  POP when stack-13=w
  ;; - POP when stack-10=w

  (run-last [] [1 4 9 1 1 6 7 5 3 1 1 1 1 4])
  (run-last [] [6 9 9 1 4 9 9 9 9 7 5 3 6 9])
  (run-last [] [6 4 9 1 4 9 9 9 9 7 5 3 1 9])

  [13 10]  ;; + PUSH 13+w

  (run-last [19] [4 9 1 4 9 9 9 9 7 5 3 1 9])
  [10 13]  ;; + PUSH 10+1

  (run-last [19 14] [9 1 4 9 9 9 9 7 5 3 1 9])
  [3 13]   ;; + PUSH 3+w

  (run-last [19 14 12] [1 4 9 9 9 9 7 5 3 1 9])
  (run-last [19 14 20] [9 4 9 9 9 9 7 5 3 1 9])
  [1 -11]  ;; - POP when stack-11 = w

  (run-last [19 14] [4 9 9 9 9 7 5 3 1 9])
  [9 11]   ;; + PUSH 9+w

  (run-last [19 14 13] [9 9 9 9 7 5 3 1 9])
  [3 -4]   ;; - POP when stack-4 = w


  (run-last [19 22] [9 9 9 7 5 3 9 9])
  [5 12]   ;; + PUSH 5+w

  (run-last [19 22 14] [9 9 7 5 3 9 9])
  (run-last [19 22 20] [9 9 7 5 9 9 9])
  [1 12]   ;; + PUSH 1+w

  (run-last [19 22 20 10] [9 7 5 9 9 9])
  (run-last [19 22 20 14] [9 7 9 9 9 9])
  [0 15]   ;; + PUSH 0+w

  (run-last [19 22 20 14 9] [7 9 9 9 9])
  (run-last [19 22 20 14 10] [8 9 9 9 9])
  (run-last [19 22 20 14 11] [9 9 9 9 9])
  [13 -2]  ;; - POP when stack-2=w

  (run-last [19 22 20 14] [9 9 9 9])
  [7 -5]   ;; - POP when stack-5=w

  (run-last [19 22 20] [9 9 9])
  [15 -11] ;; - POP when stack-11=w

  (run-last [19 22] [9 9])
  (run-last [19 22] [9 9])
  [12 -13] ;; - POP when stack-13=w

  (run-last [19] [9])
  [8 -10]  ;; - POP when stack-10=w
  )









