(ns day3.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;; ----------------------------------------
(defn read-input [file]
  (str/split-lines (slurp (io/resource file))))

(def test-input (read-input "day3/test.txt"))

(def full-input (read-input "day3/input.txt"))

;; ----------------------------------------
(defn b2-num [digits]
  (Integer/parseInt (apply str digits) 2))

(defn extract-column [lines col]
  (map #(get % col ) lines))

(defn gamma-select [cols]
  (max-key (frequencies cols) \0 \1)) ;; prefer \1

(defn gamma [input]
  (b2-num
   (for [col (range (count (first input)))]
     (gamma-select (extract-column input col)))))

(defn epsilon-select [cols]
  (min-key (frequencies cols) \1 \0)) ;; prefer \0

(defn epsilon [input]
  (b2-num
   (for [col (range (count (first input)))]
     (epsilon-select (extract-column input col)))))

(defn part1 [input]
  (* (gamma input) (epsilon input)))


;; ----------------------------------------

(defn filter-column [lines n v]
  (filter #(= v (get % n)) lines))

(defn oxygen [input]
  (loop [lines input
         col 0]
    (if (= 1 (count lines))
      (b2-num (first lines))
      (let [g (gamma-select (extract-column lines col))]
        (recur (filter-column lines col g)
               (inc col))))))

(defn co2 [input]
  (loop [lines input
         col 0]
    (if (= 1 (count lines))
      (b2-num (first lines))
      (let [e (epsilon-select (extract-column lines col))]
        (recur (filter-column lines col e)
               (inc col))))))

(defn part2 [input]
  (* (oxygen input) (co2 input)))

(comment
  (part1 test-input)
  198
  (part1 full-input)
  693486

  (part2 test-input)
  230
  (part2 full-input)
  3379326
)
