(ns day21.main
  (:require [clojure.java.io :as io]))

(def test-input [4 8])
(def full-input [7 4])

;; ---------- part1

;; move n spaces from a position and return new position
(defn move [from nspace]
  (->
    (+ from nspace)
    (dec)
    (mod 10)
    (inc)))


;; not terribly clever
(defn part1 [input]
  (loop [turn 0
         pawns input
         scores [0 0]
         player 0
         d100 (cycle (range 1 101))]
    (if (some #(>= % 1000) scores)
      (* 3 turn (apply min scores)) ;; each turn is 3 dice rolls
      (let [nspaces (reduce + (take 3 d100))
            moveto (move (nth pawns player) nspaces)
            score (+ (nth scores player) moveto)]
        (recur
          (inc turn)
          (assoc pawns player moveto)
          (assoc scores player score)
          (mod (inc player) 2)
          (drop 3 d100))))))


;; ---------- part2

;; roll value -> number of ways to get there
(def rolls
  {3 1
   4 3
   5 6
   6 7
   7 6
   8 3
   9 1})


;; apply a roll to a given position/score -
;; basically one step of part 1
(defn apply-roll [pawn scores player roll]
  (let [moveto (move (nth pawn player) roll)
        score (+' (nth scores player) moveto)]
    [(assoc pawn player moveto)
     (assoc scores player score)]))

(defn step1 [pawns scores player ntimes]
  (for [[roll nrolled] rolls]
    [(apply-roll pawns scores player roll) (*' ntimes nrolled)]))

;; do one iteration of state for a given player
(defn step [states player]
  (reduce
    (fn [m [k v]]
        (merge-with +' m {k v}))
    {}
    (apply concat
      (for [[[pawns scores] n] states]
        (step1 pawns scores player n)))))

;; for a map of states->counts, return
;; [[number-wins-p1 number-wins-p2] state-map-with-winners-removes]
(defn process-wins [states]
  [[(reduce +
      (for [[[_ [score1 score2]] n] states
            :when (>= score1 21)]
        n))
    (reduce +
      (for [[[_ [score1 score2]] n] states
            :when (>= score2 21)]
        n))]

   (into {}
     (for [[[_ [score1 score2]] n :as state] states
           :when (and (< score1 21) (< score2 21))]
       state))])


;; states is a map of
;; game state -> number of times state is seen
;; games state is: [[p1-postion p2-position] [p1-score p2-score]]
;;
;; iterate through keeping track of all possible states and the number
;; of times they are seen. remove winning states and add them to the
;; win counts
(defn part2 [input]
  (loop [states {[input [0 0]] 1}
         player 0
         turn 0
         all-wins [0 0]]
    #_(println "* " turn all-wins)
    (if (empty? states)
      (apply max all-wins)
      (let [next-states (step states player)
            [wins remaining] (process-wins next-states)]
        (recur remaining
          (mod (inc player) 2)
          (inc turn)
          (map + all-wins wins))))))


;;; ---------- results
(comment

  (part1 test-input)
  739785
  (part1 full-input)
  675024
  (part2 test-input)
  444356092776315
  (part2 full-input)
  570239341223618)
