(ns day2.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(def test-input
"forward 5
down 5
forward 8
up 3
down 8
forward 2
")

(def input-file "day2/input.txt")

(defn parse-input [text]
  (for [line (str/split-lines text)]
    (let [[dir nstr] (str/split line #"\s")]
      [dir (Integer/parseInt nstr)])))


(defn part1 [input]
  (loop [commands input
         h 0
         d 0]
    (if (empty? commands)
      (* h d)
      (let [[command n] (first commands)]
        (println "*" h d " :: " command n)
        (case command
          "forward" (recur (rest commands) (+ n h) d)
          "down" (recur (rest commands) h (+ d n))
          "up" (recur (rest commands) h (- d n)))))))

(defn part2 [input]
  (loop [commands input
         h 0
         d 0
         aim 0]
    (if (empty? commands)
      (* h d)
      (let [[command n] (first commands)]
        (case command
          "forward" (recur (rest commands)
                           (+ h n)
                           (+ d (* aim n))
                           aim)
          "down" (recur (rest commands) h d (+ aim n))
          "up" (recur (rest commands) h d (- aim n)))))))


(comment
  (part1 (parse-input test-input))
  150
  (part1 (parse-input (slurp (io/resource input-file))))
  2070300

  (part2 (parse-input test-input))
  900

  (part2 (parse-input (slurp (io/resource input-file))))
  2078985210

  ;; need a test harness or something :)
  )
