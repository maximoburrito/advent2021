(ns day6.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;; For part 1 I took the brute force approach of just calculating the fish
;;
;; For part 2 I went with a dynamic programming approach with
;; memoization.  Looking at other solutions this was clearly overkill,
;; but it was fun. :)

;;; input
(defn parse-int [nstr]
  (Integer/parseInt nstr))

(defn read-input [file]
  (map parse-int (str/split (str/trim (slurp (io/resource file))) #",")))

(def test-input (read-input "day6/test.txt"))
(def full-input (read-input "day6/input.txt"))


;;; part1
(defn decrement-fish [n]
  (if (= n 0)
    6
    (dec n)))

;; compute the next day
(defn spawn [fish]
  (let [new-fish (count (filter zero? fish))]
    (concat (map decrement-fish fish) (repeat new-fish 8))))


(defn part1 [input]
  (count (last (take (inc 80) (iterate spawn input)))))

;;; part2

(declare fishcount)

;; compute how many fish there are for one fish at fishnum state after
;; a given number of days
(defn fishcount-raw [fishnum days]
  (cond
    (zero? days) ; no days left - just the one fish
    1

    (zero? fishnum) ; fish spawns, sum our result and the new spawn result
    (+ (fishcount 8 (dec days))
       (fishcount 6 (dec days)))

    true ; same as fish-1 and day-1
    (fishcount (dec fishnum) (dec days))))

;; is there a better clojure way to recursizely memoize?
(def fishcount (memoize fishcount-raw))

(defn part2 [input]
  ;; for each fish in the input, calculate how many fish would because
  ;; at the end for just that fish, then sum that up
  (reduce +
          (map #(fishcount % 256) input)))

;;; solutions

(comment
  (part1 test-input)
  5934

  (part1 full-input)
  353274

  (part2 test-input)
  26984457539

  (part2 full-input)
  1609314870967)
