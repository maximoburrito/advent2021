(ns day17.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;; I don't feel very good about hardcoding one of the limits on my y range. I expected
;; part 2 would require me to come back and be smarter, but it didn't. I might
;; come back and improve this later


;; ---------- input

(defn parse-int [nstr]
  (Integer/parseInt nstr))

(defn parse-input [text]
  (let [[_ x1 x2 y1 y2]
        (re-matches #"target area: x=(\-?[0-9]+)..(\-?[0-9]+), y=(\-?[0-9]+)..(\-?[0-9]+)" text)]
    [(parse-int x1)
     (parse-int x2)
     (parse-int y1)
     (parse-int y2)]))

(def test-input (parse-input "target area: x=20..30, y=-10..-5"))
(def full-input (parse-input "target area: x=287..309, y=-76..-48"))

;; ---------- part1

(defn to-zero [n]
  (cond
    (< n 0) (inc n)
    (> n 0) (dec n)
    :else  n))


;; lazy (infinite sequence) firing from point p at velocity v
(defn fire [[x y :as p] [x' y' :as v]]
  (lazy-seq (cons p
              (fire
                [(+ x x')     (+ y y')]
                [(to-zero x') (dec y')]))))


;; test if point p is in target box
(defn hit [[x1 x2 y1 y2 :as target] [x y :as p]]
  (and
    (<= x1 x x2)
    (<= y1 y y2)))


;; test if point p is has entirely passed by the target
;; (assumptions about direction based on known inputs)
(defn passed [[x1 x2 y1 y2 :as target] [x y :as p]]
  (or (> x (max x1 x2))
      (> (min y1 y2) y)))

(defn part1 [[x1 x2 y1 y2 :as target]]
  (reduce max
    (for [x (range 0 (inc x2))
          y (range 500 y2 -1) ;; XXX - cheating, hard code range based on my input
          :let [path (take-while
                       #(not (passed target %))
                       (fire [0 0] [x y]))]
          :when (some #(hit target %) path)]
      (second (apply max-key second path)))))


(defn part2 [[x1 x2 y1 y2 :as target]]
  (count
    (for [x (range 0 (inc x2))
          y (range 500 (dec y1) -1) ;; also cheating here
          :let [path (take-while
                       #(not (passed target %))
                       (fire [0 0] [x y]))]
          :when (some #(hit target %) path)]
      [x y])))


;; ---------- result
(comment
  (part1 test-input)
  45
  (part1 full-input)
  2850

  (part2 test-input)
  112
  (part2 full-input)
  1117)
