(ns day13.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.set :as set]))

;;; ---------- input

(defn resource [name]
  (slurp (io/resource (format "day13/%s.txt" name))))

(defn parse-int [nstr]
  (Integer/parseInt nstr))

(defn parse-point [line]
  (->> (str/split line #",")
    (map parse-int)
    vec))

(defn parse-fold [line]
  (let [[_ xy num](re-matches #"fold along (.)=(.+)" line)]
    [(keyword xy) (parse-int num)]))

(defn parse [text]
  (loop [lines (str/split-lines text)
         points #{}
         folds []]
    (let [line (first lines)]
      (cond
        (nil? line)
        {:points points :folds folds}

        (empty? line)
        (recur (rest lines) points folds)

        (str/starts-with? line "fold")
        (recur
          (rest lines)
          points
          (conj folds (parse-fold line)))

        :else
        (recur
          (rest lines)
          (conj points (parse-point line))
          folds)))))

(def test-input (parse (resource "test")))
(def full-input (parse (resource "input")))

;;; ---------- part1

(defn do-fold [points [fold-type nfold]]
  (into #{}
    (for [[x y] points]
      (if (= :y fold-type)
        (cond
          (< y nfold)
          [x y]
          (> y nfold)
          [x (- (* 2 nfold) y)])
        (cond
          (< x nfold)
          [x y]
          (> x nfold)
          [(- (* 2 nfold) x) y])))))

(defn part1 [input]
  (let [fold (first (:folds input))]
    (count (do-fold (:points input) fold))))

;;; ---------- part2

(defn dump [points]
  (for [y (range 6)]
    (apply str
      (for [x (range 40)]
        (if (points [x y]) "X" " ")))))

(defn part2 [input]
  (dump
    (reduce do-fold (:points input) (:folds input))))


;;; ---------- results

(comment
  (part1 test-input)
  17

  (part1 full-input)
  795

  (part2 test-input)
  ("XXXXX                                   "
   "X   X                                   "
   "X   X                                   "
   "X   X                                   "
   "XXXXX                                   "
   "                                        ")

  (part2 full-input) ;; CEJKLUGJ
  (" XX  XXXX   XX X  X X    X  X  XX    XX "
   "X  X X       X X X  X    X  X X  X    X "
   "X    XXX     X XX   X    X  X X       X "
   "X    X       X X X  X    X  X X XX    X "
   "X  X X    X  X X X  X    X  X X  X X  X "
   " XX  XXXX  XX  X  X XXXX  XX   XXX  XX  ")

  )
