(ns day8.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.set :as set]))



;;; parse
(defn read-file [file]
  (slurp (io/resource file)))

(defn parse-line [line]
  (let [[pattern-part code-part] (str/split line #" \| ")]
    [(str/split pattern-part #" ")
     (str/split code-part #" ")]))

(defn parse-input [text]
  (mapv parse-line (str/split-lines text)))

(def test-input (parse-input (read-file "day8/test.txt")))
(def full-input (parse-input (read-file "day8/input.txt")))

;;; part 1

(defn test-1478 [code]
  (#{2 3 4 7} (count code)))

(defn part1 [input]
  (count
   (for [line input
         :let [[patterns codes] line]
         code codes
         :when (test-1478 code)]
     code)))


;;; part 2

(defn filter-n [n]
  #(= n (count %)))

(defn charset [word]
  (into #{} (seq word)))

(defn diff [p1 p2]
  (apply str (set/difference (charset p1) (charset p2))))

(defn has-all [desired]
  (let [goal-chars (charset desired)]
    (fn [pattern]
      (every? (charset pattern) goal-chars))))


;; return a codebook of a pattern-set => digit
;; sets are function, and clojure can match on sets
(defn make-codebook [patterns]
  (let [p1 (first (filter (filter-n 2) patterns))
        p4 (first (filter (filter-n 4) patterns))
        p7 (first (filter (filter-n 3) patterns))
        p8 (first (filter (filter-n 7) patterns))
        p069 (filter (filter-n 6) patterns)
        p0 (first (remove (has-all (diff p4 p1)) p069))
        p69 (filter (has-all (diff p4 p1)) p069)
        p9 (first (filter (has-all p1) p69))
        p6 (first (remove (has-all p9) p69))
        p235 (filter (filter-n 5) patterns)
        p5 (first (filter (has-all (diff p4 p1)) p235))
        p23 (remove (has-all p5) p235)
        p3 (first (filter (has-all p1) p23))
        p2 (first (remove (has-all p3) p23))]

    {(charset p0) 0
     (charset p1) 1
     (charset p2) 2
     (charset p3) 3
     (charset p4) 4
     (charset p5) 5
     (charset p6) 6
     (charset p7) 7
     (charset p8) 8
     (charset p9) 9}))


;; given a codebook and vector of encoded segments,
;; decode the digits and construct the number
(defn decode [codebook encoded]
  (Integer/parseInt (apply str (map #(codebook (charset %)) encoded))))

(defn part2 [input]
  (reduce +
          (for [[patterns codes] input
                :let [codebook (make-codebook patterns)
                      n (decode codebook codes)]]
            n)))


;;; results

(comment
  (part1 test-input)
  26
  (part1 full-input)
  301

  (part2 test-input)
  61229
  (part2 full-input)
  908067)
