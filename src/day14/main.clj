(ns day14.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;;; ---------- parsing
(defn parse-rule [line]
  (let [[_ from to] (re-matches #"(..) -> (.)" line)]
    {from to}))

(defn parse [text]
  (let [lines (str/split-lines text)
        pattern (first lines)
        rules (reduce merge (map parse-rule (drop 2 lines)))]

    {:pattern pattern
     :rules rules}))

(defn read-file [file]
  (slurp (io/resource (format "day14/%s.txt" file))))

(def test-input (parse (read-file "test")))
(def full-input (parse (read-file "input")))

;;; ---------- part 1
(defn expand [pattern rules ]
    (let [mixes (map #(apply str %) (partition 2 1 pattern))
        r (map rules mixes)]
      (apply str (concat (interleave pattern r) [(last pattern)]))))


(defn score [freqs]
  (- (apply max (vals freqs)) (apply min (vals freqs))))


(defn part1
  ([input]
   (part1 input 10))
  ([{:keys [pattern rules]} nsteps]
   (let [step #(expand % rules)]
     (score (frequencies (nth (iterate step pattern) nsteps))))))


;;; ---------- part2
(def merge+ (partial merge-with +))

(defn expand2 [counts rules]
  (reduce merge+
    (for [[pattern n] counts
         :let [[c1 c2] pattern
               replacement (rules pattern)]]
     {(str c1 replacement) n
      (str replacement c2) n})))


(def charset (map char (range (int \A) (inc (int \Z)))))


;; this code is a mess, but the basic idea is that that for each iteration
;; I'm keeping a map of all the pairs and their occurances ex: {"NN" 1 "NB" 2 ...}
;; On a step of AB -> C with {"AB" n} the next iteration contains {"AC" n "CB n}
;; using merge+ above to merge the results.
;; the tricky part was going back from the pairs freq to the letter freqencies.
;; I'm so tired now that I'm probably missing the obvious, but expirementall I
;; could see that the current final value for character "A" was the max
;; of the number of times it appeared in a pair A? and the number of times it appeared
;; in a pair ?A.

(defn part2
  ([input]
   (part2 input 40))
  ([{:keys [pattern rules]} nsteps]
   (let [freqs
         (frequencies
           (for [pair (partition 2 1 pattern)]
             (apply str pair)))

         step
         #(expand2 % rules)

         last-freqs
         (nth (iterate step freqs) nsteps)]

     (score
       (reduce merge+
         (for [c charset
               :let [n (apply max (reduce
                                    (fn [count1 count2]
                                      (map + count1 count2))
                                    (for [[[c1 c2] n] last-freqs]
                                      [(if (= c1 c) n 0)
                                       (if (= c2 c) n 0)])))]
               :when (> n 0)]
           {c n}))))))

;;; ---------- results

(comment
  (part1 test-input)
  1588
  (part1 full-input)
  3118
  (part2 test-input)
  2188189693529
  (part2 full-input)
  4332887448171
)
