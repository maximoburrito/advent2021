(ns day19.main
  (:require [clojure.java.io :as io]
            [clojure.set :as set]
            [clojure.string :as str]))

;; This is the second worst piece of code I've written in all my
;; years of advent. It's utter garbage, and I have zero motivation
;; to even clean it up. The code takes 20 minutes or so to run.  100% meh on
;; my part. Next please...

(defn parse-number [nstr]
  (Integer/parseInt nstr))

(defn parse-line [line]
  (mapv parse-number (str/split line #",")))

(defn parse-section [text]
  (map parse-line (rest (str/split-lines text))))

(defn parse-input [text]
  (map parse-section (str/split text #"\n\n")))

(def test-input (parse-input (slurp (io/resource "day19/test.txt"))))
(def full-input (parse-input (slurp (io/resource "day19/input.txt"))))

(defn distance [[x1 y1 z1 :as p1] [x2 y2 z2 :as p2]]
  (sort (map #(Math/abs %) (map - p1 p2))))

(defn distances [scan p1]
  (into #{}
    (for [p2 scan
          :when (not= p1 p2)]
      (distance p1 p2))))

(defn matching-point [scan-a scan-b p-a]
  (first
    (apply max-key second
     (for [p-b scan-b]
       [p-b (count
              (set/intersection
                (distances scan-a p-a)
                (distances scan-b p-b)))]))))

;; for two scans, the number of points in c
(defn overlap-p [scan-a scan-b p-a]
  (reduce max
    (map
      #(count
         (set/intersection
           (distances scan-a p-a)
           (distances scan-b %)))
      scan-b)))



(defn overlap [scan-a scan-b]
  (for [p scan-a
        :let [n (overlap-p scan-a scan-b p)]
        :when (> n 0)]
    [p (matching-point scan-a scan-b p)]))


(def colperms (for [x (range 3)
                    y (range 3)
                    :when (not= x y)
                    z (range 3)
                    :when (and (not= z y) (not= z x))]
                [x y z]))

(def dirperms (for [x [1 -1]
                    y [1 -1]
                    z [1 -1]]
                [x y z]))

(defn most-frequent [vs]
  (let [fs (frequencies vs)]
    (reduce #(max-key fs %1 %2) (keys fs))))

(defn orient [overlaps]
  (let [choices
        (for [[a b] overlaps
              colperm colperms
              dirperm dirperms
              :let [b' (mapv * dirperm
                         [(nth b (colperm 0))
                          (nth b (colperm 1))
                          (nth b (colperm 2))])]]
          [(mapv - a b') colperm dirperm])

        best-origin (most-frequent (map first choices))]
    (first
      (for [[origin _ _ :as choice] choices
            :when (= best-origin origin)]
        choice))))


(defn transform [p [offset colperm dirperm]]
  (->> [(nth p (colperm 0))
        (nth p (colperm 1))
        (nth p (colperm 2))]
    (mapv * dirperm)
    (mapv + offset)))

(defn part1 [input]
  (loop [scans (into #{} (rest input))
         all-points (into #{} (first input))]
    (println "!!!" (count scans) (count all-points))
    (if (empty? scans)
      all-points
      (let [[scan orientation]
            (first
              (for [scan scans
                    :let [overlaps (overlap (into [] all-points) scan)]
                    :when (> (count (seq overlaps)) 10)]
                [scan (orient overlaps) (count overlaps)]))

            new-points
            (for [p scan] (transform p orientation))]

        (recur
          (remove #(= scan %) scans)
          (into all-points new-points))))))


(defn mandist [p1 p2]
  (reduce + (map #(Math/abs %)(map - p1 p2))))

(defn answer [scanners]
  (reduce max
    (for [scanner1 scanners
         scanner2 scanners]
     (mandist scanner1 scanner2))))

(defn part2 [input]
  (loop [scans (into #{} (rest input))
         all-points (into #{} (first input))
         orientations [[0 0 0]]]
    (println "!!!" (count scans) (count all-points) orientations)
    (if (empty? scans)
      (answer orientations)
      (let [[scan orientation]
            (first
              (for [scan scans
                    :let [overlaps (overlap (into [] all-points) scan)]
                    :when (> (count (seq overlaps)) 10)]
                [scan (orient overlaps) (count overlaps)]))

            new-points
            (for [p scan] (transform p orientation))]
        (recur
          (remove #(= scan %) scans)
          (into all-points new-points)
          (conj orientations (first orientation)))))))
