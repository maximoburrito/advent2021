(ns day25.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(defn parse-input [text]
  (let [lines (str/split-lines text)]
    (into {}
      (for [y (range (count lines))
            :let [line (nth lines y)]
            x (range (count line))
            :let [c (nth line x)]]

        [[x y] c]))))

(def test-input (parse-input (slurp (io/resource "day25/test.txt"))))
(def full-input (parse-input (slurp (io/resource "day25/input.txt"))))


;; needing this function is my biggest regret
(defn sea-size [sea]
  [(inc (reduce max (map first (keys sea))))
   (inc (reduce max (map second (keys sea))))])

(defn dump [sea]
  (println
    (let [[sx sy] (sea-size sea)]
     (str/join "\n"
       (for [y (range sy)]
         (apply str
           (for [x (range sx)]
             (sea [x y]))))))))

(defn apply-move [sea [from to]]
  (-> sea
    (assoc to (sea from))
    (assoc from (sea to))))

(defn apply-moves [sea moves]
  (reduce apply-move sea moves))

(defn move [sea ctype [dx dy]]
  (let [[sx sy] (sea-size sea)
        moves (for [[x y :as pos] (keys sea)
                    :when (= ctype (sea pos))
                    :let [dest [(mod (+ x dx) sx)
                                (mod (+ y dy) sy)]]
                    :when (= \. (sea dest))]
                [pos dest])]
    (apply-moves sea moves)))

(defn step [sea]
  (-> sea
    (move \> [1 0])
    (move \v [0 1])))

(defn part1 [input]
  (loop [sea input
         n 1]
    (let [next-sea (step sea)]
      (if (= sea next-sea)
        n
        (recur next-sea (inc n))))))


(comment
  (part1 test-input)
  58
  (part1 full-input)
  598)
