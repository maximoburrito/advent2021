(ns day1.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(defn to-ints [strs]
  (map #(Integer/parseInt %) strs))

(defn read-input [file]
  (-> (io/resource file)
      (slurp)
      (str/split #"\n")
      (to-ints)))

(def input-file "day1/input.txt")

(def test1 [199 200 208 210 200 207 240 269 260 263])

(defn part1 [input]
  (loop [nums (rest input)
         count 0
         prev (first input)]
    (if (empty? nums)
      count

      (let [current (first nums)
            new-count (if (> current prev) (inc count) count)]
        (recur (rest nums)
               new-count
               (first nums))))))

(defn part2 [input]
  (let [windows (partition 3 1 input)
        sums (map #(apply + %) windows)]
    (part1 sums)))



(comment
  (part1 (read-input input-file))
  1527

  (part2 (read-input input-file))
  1575

  )
