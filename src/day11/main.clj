(ns day11.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;;; ---------- parse

(defn parse-input [text]
  (into {}
    (let [lines (str/split-lines text)]
      (for [y (range (count lines))
            :let [line (nth lines y)]
            x (range (count line))]
        [[x y] (- (int (nth line x))
                 (int \0))] ))))

(defn read-file [file]
  (slurp (io/resource file)))

(def test-input (parse-input (read-file "day11/test.txt")))
(def full-input (parse-input (read-file "day11/input.txt")))

;;; ---------- part1

(defn dump [state]
  (let [[maxx maxy] (last (sort (keys state)))]
    (str/join
      "\n"
      (for [y (range (inc maxy))]
       (apply str
         (for [x (range (inc maxx))]
           (state [x y])))))))

;; add one to every point in points
(defn addenergy [state points]
  (reduce #(update %1 %2 inc) state points))

;; add one to every point
(defn addenergy-all [state]
  (addenergy state (keys state)))

(defn zero-high-energy [energy]
  (if (> energy 9)
    0
    energy))

;; zero out any point that is over enery
(defn zero-flashed [state]
  (reduce #(update %1 %2 zero-high-energy) state (keys state)))

(defn neighbors-of [[x y]]
  [[(dec x) (dec y)]
   [(dec x) y]
   [(dec x) (inc y)]
   [x (dec y)]
   [x (inc y)]
   [(inc x) (dec y)]
   [(inc x) y]
   [(inc x) (inc y)]])

(defn advance [in-state]
  (loop [state (addenergy-all in-state)
         to-check (keys in-state) ; points that need to be checked
         flashed #{}]             ; points that have already flashed
    (let [p (first to-check)]
      (cond
        ;; no more points to check, zero the points that flashed and return count
        (nil? p)
        [(zero-flashed state) (count flashed)]

        ;; p has already flashed or doesn't have the energy
        (or (flashed p) (<= (state p) 9))
        (recur state (rest to-check) flashed)

        ;; this is a flash, get all neighbors, bump their energy
        ;; and add those points to the list to be checked
        :else
        (let [neighbors (filter state (neighbors-of p))]
          (recur
            (addenergy state neighbors)
            (concat to-check neighbors)
            (conj flashed p)))))))

(defn part1 [input]
  (loop [iteration 1
         state input
         flashes 0]
    #_(println "------------- " iteration flashes)
    #_(println (dump state))

    ;; loop through advancing states, accumulating the number of flashes
    (if (> iteration 100)
      flashes
      (let [[newstate nflashes] (advance state)]
        (recur
          (inc iteration)
          newstate
          (+ flashes nflashes))))))

;;; ---------- part2

(defn part2 [input]
  (loop [iteration 0
         state input]
    (let [[newstate nflashes] (advance state)]
      ;; keep advancing the state until all there is one flash for every state
      (if (= nflashes (count state))
        (inc iteration)
        (recur (inc iteration) newstate)))))

;;; ---------- results

(comment
  (part1 test-input)
  1656
  (part1 full-input)
  1705
  (part2 test-input)
  195
  (part2 full-input)
  265
)
