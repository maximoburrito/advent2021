(ns day7.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;;; parse

(defn parse-int [nstr]
  (Integer/parseInt nstr))

(defn read-input [file]
  (map parse-int
       (-> (slurp (io/resource file))
           (str/trim)
           (str/split #","))))

(def test-input (read-input "day7/test.txt"))
(def full-input (read-input "day7/input.txt"))

;;; part1

;; find the score for moving everything to position n
(defn part1-score [nums n]
  (reduce + (map #(Math/abs (- % n)) nums)))

(defn part1 [input]
  (let [search-range (range (apply min input)
                            (inc (apply max input)))]
    (reduce min
            (for [i search-range]
              (part1-score input i)))))


;;; part2

;; the cost of step n is 1+2+...n = n * (n + 1) /2
(defn step-cost [n]
  (/ (* n (inc n))
       2))

;; compute score as before, but adjusting the core
;; by the step-cost
(defn part2-score [nums n]
  (reduce + (map #(step-cost (Math/abs (- % n))) nums)))

(defn part2 [input]
  (let [search-range (range (apply min input)
                            (inc (apply max input)))]
    (apply min
           (for [i search-range]
             (part2-score input i)))))


;;; results
(comment
  (part1 test-input)
  37
  (part1 full-input)
  352254

  (part2 test-input)
  168
  (part2 full-input)
  99053143)
