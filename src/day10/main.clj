(ns day10.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;;; inputs
(defn parse-input [text]
  (str/split-lines text))

(def test-input (parse-input (slurp (io/resource "day10/test.txt"))))
(def full-input (parse-input (slurp (io/resource "day10/input.txt"))))


;;; part1
(def pairs
  {\( \)
   \[ \]
   \< \>
   \{ \}})


(defn first-error [line]
  ;; find and return the first error in the line,
  ;; keep unmatched symbols on the stack
  (loop [syms (seq line)
         stack []]
    (let [sym (first syms)]
      (cond
        ;; no more input, no errors
        (nil? sym)
        nil

        ;; if we're an open symbol, push the symbol on the stack
        (pairs sym)
        (recur (rest syms)
               (conj stack sym))

        ;; we're a closing symbol but there's nothing on the stack to match
        ;; not sure if we see this in the problem
        (empty? stack)
        sym

        ;; we're the expected closing symbol - pop the stack
        (= sym (pairs (peek stack)))
        (recur (rest syms)
               (pop stack))

        ;; we're an unexpected closing symbol, here be the match
        :else
        sym))))

(def points
  {\) 3
   \] 57
   \} 1197
   \> 25137})

(defn score-part1 [line]
  ;; simple map lookup for score on the first error
  (or (points (first-error line)) 0))

(defn part1 [input]
  (reduce + (map score-part1 input)))


;;; part2

(defn complete-line [line]
  ;; same structure as part 1
  (loop [syms (seq line)
         stack []]
    (let [sym (first syms)]
      (cond
        ;; we've run out of input
        ;; play the stack back in reverse to get the completion
        (nil? sym)
        (apply str (map pairs (reverse stack)))

        ;; an open symbol, push on the stack
        (pairs sym)
        (recur (rest syms)
               (conj stack sym))

        ;; closing symbol, stack is empty - exit with no result
        (empty? stack)
        nil

        ;; closing symbol matches, pop the stack
        (= sym (pairs (peek stack)))
        (recur (rest syms)
               (pop stack))

        ;; closing symbol no match, exit with no result
        :else
        nil))))


(def part2-points
  {\) 1
   \] 2
   \} 3
   \> 4})

(defn score-completion [completion]
  (let [add-to-score
        (fn [score sym]
          (+ (* score 5) (part2-points sym)))]
    ;; the odd place where reduce makes more since than loop/recur
    ;; accumulate the score character by character
    (reduce add-to-score 0 (seq completion))))

(defn middle-val [ns]
  ;; ns is always odd and sorted, take the middle value
  (first (drop (int (/ (count ns) 2))
               ns)))

(defn part2 [input]
  (let [scores (for [line input
                     :let [completion (complete-line line)]
                     :when completion]
                 (score-completion completion))]
    (middle-val (sort scores))))

;;; results

(comment
  (part1 test-input)
  26397
  (part1 full-input)
  321237

  (part2 test-input)
  288957
  (part2 full-input)
  2360030859)
