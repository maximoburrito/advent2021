(ns day9.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))
;;; input
(defn read-file [file]
  (slurp (io/resource file)))

(defn parse-input [text]
  (let [lines (str/split-lines text)
        yrange (count lines)
        xrange (count (first lines))
        cave  (into {}
                    (for [y (range yrange)
                          :let [line (nth lines y)]
                          x (range (count line))]
                      [[x y] (- (int (nth line x)) (int \0))]))]
    cave))

(def test-input (parse-input (read-file "day9/test.txt")))
(def full-input (parse-input (read-file "day9/input.txt")))

;;; part1

(defn neighbors-of [[x y]]
  [[(inc x) y]
   [(dec x) y]
   [x (inc y)]
   [x (dec y)]])

(defn low-points [cave]
  ;; for each point in the cave, when all my neighbors are greater than me
  (for [point (keys cave)
        :let [me (cave point)
              neighbors (filter identity (map cave (neighbors-of point)))]
        :when (every? #(> % me) neighbors)]
    point))

(defn part1 [input]
  (let [points (low-points input)]
    ;; sum the points and add one for each point
    (reduce +
            (count points) ;;
            (map input points))))


;;; part 2
(defn fill [cave low-point]
  ;; simple recur, start at a point and
  ;; visit all neighbors of that point that are < 9

  (loop [to-visit [low-point]
         visited #{}]
    (let [current (peek to-visit)]
      (cond
        ;; nothing left to visit, return the size of the basin
        (nil? current)
        (count visited)

        ;; we've already been here, move on
        (visited current)
        (recur (pop to-visit) visited)

        ;; we have work to do, find all the unvisited neighbors that
        ;; are in the basin. Add them to the visit list, marking
        ;; this location as visited
        :else
        (let [neighbors (for [p (neighbors-of current)
                              :when (and (not (visited p))
                                         (cave p)
                                         (< (cave p) 9))]
                          p)]
          (recur (into (pop to-visit) neighbors)
                 (conj visited current)))))))


(defn part2 [input]
  (let [basins (for [point (low-points input)]
                 (fill input point))

        top3   (take-last 3 (sort basins))]
    ;; multiply the top 3 basin sizes
    (reduce * top3)))

;;; results

(comment
  (part1 test-input)
  15
  (part1 full-input)
  564

  (part2 test-input)
  1134
  (part2 full-input)
  1038240

  )
