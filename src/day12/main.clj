(ns day12.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.set :as set]))

;;; ---------- input
(defn make-input [input]
  (slurp (io/resource (format "day12/%s.txt" input))))

(defn parse-line [line]
  (str/split line #"\-"))

(defn parse-input [text]
  (map parse-line (str/split-lines text)))

(def test-input (parse-input (make-input "test")))
(def full-input (parse-input (make-input "input")))


;;; ---------- part 1
(defn neighbors-of [graph me]
  (let [my-neighbor (fn [[e1 e2]]
      (cond
        (= e1 me) e2
        (= e2 me) e1))]
    (filter identity (map my-neighbor graph))))

(defn small-cave [node]
  (= node (str/lower-case node)))

(defn valid-path [path]
  (every?
    (fn [[node seen]]
      (not (and (small-cave node) (> seen 1))))
    (frequencies path)))

(defn complete-path [path]
  (= "end" (last path)))

(defn part1 [input]
  ;; each loop, expand ALL the paths we are exploring
  ;; paths that reach the end get added to the final paths set
  ;; paths that are valid become the next rounds exploring set
  ;; assume no big-cave loops
  (loop [paths #{}
         exploring #{["start"]}]
    (if (empty? exploring)
      (count paths)
      (let [expanded-paths
            (for [current exploring
                  neighbor (neighbors-of input (last current))
                  :let [expanded (conj current neighbor)]
                  :when (valid-path expanded)]
              expanded)]
        (recur
          (into paths (filter complete-path expanded-paths))
          (into #{} (remove complete-path expanded-paths)))))))

;; ---------- part 22

(defn start-end [node]
  (#{"start" "end"} node))

(defn small-inner-cave [node]
  (and (small-cave node)
    (not (start-end node))))

;; uggh - this code is ugly and slow, I should revisit this
(defn valid-path2 [path]
  (let [freqs (frequencies path)
        smalls (filter small-inner-cave (keys freqs))
        freq-not-one #(not= 1 (freqs %))]
    (cond
      (or
        (> (or (freqs "start") 0) 1)
        (> (or (freqs "end") 0) 1))
      false

      (seq (filter #(> (freqs %) 2) smalls))
      false

      (> (count (filter freq-not-one smalls)) 1)
      false

      :else
      true)))


;; same as part1 except the call to valid-path2
(defn part2 [input]
  (loop [paths #{}
         exploring #{["start"]}]
    (if (empty? exploring)
      (count paths)
      (let [expanded-paths
            (for [current exploring
                  neighbor (neighbors-of input (last current))
                  :let [expanded (conj current neighbor)]
                  :when (valid-path2 expanded)]
              expanded)]
        (recur
          (into paths (filter complete-path expanded-paths))
          (into #{} (remove complete-path expanded-paths)))))))

;;; results

(comment
  (part1 test-input)
  10
  (part1 full-input)
  4411

  (part2 test-input)
  36
  (part2 full-input)
  136767

  )

