(ns day23.part1
  (:require [clojure.java.io :as io]
            [clojure.data.priority-map :as pmap]))


;; ---------- inputs

(def test-input
  {:A [:a0 :d0]
   :B [:a1 :c1]
   :C [:b1 :c0]
   :D [:b0 :d1]
   :cost 0})

(def full-input
  {:A [:c0 :d1]
   :B [:a0 :b1]
   :C [:d0 :a1]
   :D [:b0 :c1]
   :cost 0})

;; ---------- part1


(def all-amphipods
  (for [a-type [:A :B :C :D]
        which [0 1]]
    [a-type which]))

(def all-hallways #{:h0 :h1 :h2 :h3 :h4 :h5 :h6 :h7 :h8 :h9 :h10})

(def hallway-for
  {:a0 :h2
   :a1 :h2
   :b0 :h4
   :b1 :h4
   :c0 :h6
   :c1 :h6
   :d0 :h8
   :d1 :h8})

(def goals-for
  {:A [:a0 :a1]
   :B [:b0 :b1]
   :C [:c0 :c1]
   :D [:d0 :d1]})

(def primary   {:a1 :a0 :b1 :b0 :c1 :c0 :d1 :d0})
(def secondary {:a0 :a1 :b0 :b1 :c0 :c1 :d0 :d1})

(defn equals [v]
  (partial = v))

;; is this a hallway position
(defn is-hallway [position]
  (all-hallways position))

(defn hallway-number [hallway]
  (Integer/parseInt (.substring (name hallway) 1)))

;; is the
(defn type-at [target-position input]
  (first
    (for [[a-type positions] (dissoc input :cost)
          :when (some (equals target-position) positions)]
      a-type)))

(defn is-target [a-type position input]
  (let [[primary secondary] (goals-for a-type)]
    (or
      (= primary position)
      (and (= secondary position) (= a-type (type-at primary input))))))



(defn drange [i j]
  (if (< i j)
    (range i (inc j))
    (range i (dec j) -1)))

(defn make-path [from to]
  (concat
    (when (not (is-hallway from))
      (if (secondary from)
        [from (secondary from)]
        [from]))

    (let [nfrom (hallway-number (if (is-hallway from)
                                  from
                                  (hallway-for from)))
          nto (hallway-number (if (is-hallway to)
                                to
                                (hallway-for to)))]
      (mapv #(keyword (str "h" %)) (drange nfrom nto)))

    (when (not (is-hallway to))
      (if (secondary to)
        [(secondary to) to]
        [to]))))



(defn can-move [path state]
  (every? #(nil? (type-at % state)) (rest path)))

(defn moves [[a-type which :as amphipod] state]
  (let [position (get-in state amphipod)]
    (cond
      (is-hallway position)
      (let [[inner outer] (goals-for a-type)
            dest (cond
                   (type-at outer state)
                   nil

                   (type-at inner state)
                   (when (= a-type (type-at inner state))
                     outer)

                   :else
                   inner)]
        (when dest
          (let [path (make-path position dest)]
            (when (can-move path state)
              [dest]))))

      ;; :at-position. no moves
      (is-target a-type position state)
      nil

      ;; wrong room and blocked
      (or
        (and (= :a0 position) (type-at :a1 state))
        (and (= :b0 position) (type-at :b1 state))
        (and (= :c0 position) (type-at :c1 state))
        (and (= :d0 position) (type-at :d1 state)))
      nil

      ;; wrong room, move to hallway
      :else
      (for [hallway #{:h0 :h1 :h3 :h5 :h7 :h9 :h10}
            :let [path (make-path position hallway)]
            :when (can-move path state)]
        hallway))))



(defn amphipod-cost [[a-type _ :as amphipod]]
  (get {:A 1 :B 10 :C 100 :D 1000} a-type))

(defn all-moves [state]
  (for [amphipod all-amphipods
        target (moves amphipod state)
        :let [path (make-path (get-in state amphipod) target)
              cost (* (amphipod-cost amphipod)
                      (dec (count path)))]]
    (-> state
      (update :cost + cost)
      (assoc-in amphipod target))))

(defn win? [state]
  (and
    (#{[:a0 :a1] [:a1 :a0]} (:A state))
    (#{[:b0 :b1] [:b1 :b0]} (:B state))
    (#{[:c0 :c1] [:c1 :c0]} (:C state))
    (#{[:d0 :d1] [:d1 :d0]} (:D state))))

(defn dump [state]
  (let [t (fn [pos] (name (or (type-at pos state) ".")))]
    (println "#############")
    (println (apply str "#"
               (t :h0) (t :h1) (t :h2)
               (t :h3) (t :h4) (t :h5)
               (t :h6) (t :h7) (t :h8)
               (t :h9) (t :h10) "#"))
    (println (apply str "###" (t :a1) "#" (t :b1) "#" (t :c1) "#" (t :d1) "###"))
    (println (apply str "  #" (t :a0) "#" (t :b0) "#" (t :c0) "#" (t :d0) "#"))
    (println "  #########")))

(defn dump2 [state]
  (let [t (fn [pos] (name (or (type-at pos state) ".")))]
    (println (apply str " "
               (t :h0) (t :h1) (t :h2)
               (t :h3) (t :h4) (t :h5)
               (t :h6) (t :h7) (t :h8)
               (t :h9) (t :h10) " "))
    (println (apply str "   " (t :a1) " " (t :b1) " " (t :c1) " " (t :d1) "   "))
    (println (apply str "   " (t :a0) " " (t :b0) " " (t :c0) " " (t :d0) " "))))

(defn part1 [input]
  (loop [states (pmap/priority-map input 0)
         best 15000
         i 1]
    (let [[state cost] (peek states)]
      (when (= 0 (mod i 2000))
        (println "* " best (count states) "current=" cost))

      (cond
        (nil? state)
        best

        (> cost best)
        best

        (win? state)
        (recur
          (pop states)
          cost
          (inc i))

        :else
        (recur
          (reduce
            #(assoc %1 %2 (:cost %2))
            (pop states)
            (all-moves state))
          best
          (inc i))))))


(defn follow-one [state]
  (Thread/sleep 10)
  (println "*" (boolean (win? state)) state)

  (when (not (win? state))
    (when-let [next-state (second (all-moves state))]
      (recur next-state))))

(def play-atom (atom nil))

(defn play
  ([n]
   (swap! play-atom #(nth (all-moves %) n))
   (play))
  ([]
   (let [states (all-moves @play-atom)]
     (doseq [i (range (count states))]
       (let [state (nth states i)]
         (println "====" i (:cost state))
         (dump2 state)
         (println))))))

(defn start-play [state]
  (reset! play-atom state)
  (play))


