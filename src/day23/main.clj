(ns day23.main
  (:require [clojure.java.io :as io]
            [clojure.data.priority-map :as pmap]))


;; this is a completely new implementation from part1, which is in day23.part1
;; it's not completely generic (some places still assume the room size)
;; This code still could use another full rewrite, but it's definitely
;; better than the part 1 code

;; ---------- inputs

(def test-input
  {:room-a [:a :d :d :b]
   :room-b [:d :b :c :c]
   :room-c [:c :a :b :b]
   :room-d [:a :c :a :d]
   :hall [nil nil nil nil nil nil nil nil nil nil nil]})

(def full-input
  {:room-a [:b :d :d :c]
   :room-b [:d :b :c :b]
   :room-c [:a :a :b :d]
   :room-d [:c :c :a :a]
   :hall [nil nil nil nil nil nil nil nil nil nil nil]})

;; ---------- part2

(def amphipod-cost
  {:a 1 :b 10 :c 100 :d 1000})

(def target-room
  {:a :room-a
   :b :room-b
   :c :room-c
   :d :room-d})

(def target-amphipod
  {:room-a :a
   :room-b :b
   :room-c :c
   :room-d :d})

(def hallway-for
  {:room-a 2
   :room-b 4
   :room-c 6
   :room-d 8})

;; steps from hallway to most packed
;; position in room - nil if move not allowed
(defn steps-to-pack-room [state room-name]
  (loop [spots (get state room-name)
          steps 4]
    (cond
      (empty? spots)
      nil

      (nil? (first spots))
      steps

      (= (target-amphipod room-name)
        (first spots))
      (recur (rest spots)
        (dec steps))

      :else
      nil)))

(defn hall-path [i j]
  (if (< i j)
    (range (inc i) (inc j))
    (range (dec i) (dec j) -1)))

(defn hall-steps [state hall-from hall-to]
  (let [path (hall-path hall-from hall-to)]
    (when (every? #(nil? (get-in state [:hall %])) path)
      (count path))))


(defn pack-in [room amphipod]
  (when-let [pos (first
                   (for [i (range (count room))
                         :when (nil? (nth room i))]
                     i))]
    (assoc room pos amphipod)))


;; [amphipod position steps] or nil
;; only when amphipod wants to leave
(defn top-amphipod [state room-name]
  (let [room (get state room-name)
        desired (target-amphipod room-name)
        wants-to-leave (not-every? #(or (nil? %) (= desired %)) room)]
    (when wants-to-leave
      (first
        (for [i (reverse (range (count room)))
              :let [amphipod (nth room i)]
              :when amphipod]
          [amphipod i (- (count room) i)])))))

;; return map of states -> cost
(defn all-moves [state]
  (into {}
    (concat
      ;; move things in hall to a final room
      (for [hall (range (count (:hall state)))
            :let [amphipod (get-in state [:hall hall])]
            :when amphipod
            :let [room (target-room amphipod)
                  room-steps (steps-to-pack-room state room)]
            :when room-steps
            :let [hallway-steps (hall-steps state hall (hallway-for room))]
            :when hallway-steps
            :let [total-cost (*
                               (amphipod-cost amphipod)
                               (+ hallway-steps room-steps))]]
        [(-> state
           (assoc-in [:hall hall] nil)
           (update room pack-in amphipod))
         total-cost])

      ;; move things at top of room to hall
      (for [room-name [:room-a :room-b :room-c :room-d]
            :let [[amphipod pos room-steps] (top-amphipod state room-name)]
            :when amphipod
            hall [0 1 3 5 7 9 10]
            :let [hallway-steps (hall-steps state (hallway-for room-name) hall)]
            :when hallway-steps
            :let [total-cost (*
                               (amphipod-cost amphipod)
                               (+ hallway-steps room-steps))]]
        [(-> state
           (assoc-in [:hall hall] amphipod)
           (assoc-in [room-name pos] nil))
         total-cost]))))

(defn win? [state]
  (and
    (= [:a :a :a :a] (:room-a state))
    (= [:b :b :b :b] (:room-b state))
    (= [:c :c :c :c] (:room-c state))
    (= [:d :d :d :d] (:room-d state))))


(defn part2 [input]
  (loop [states (pmap/priority-map input 0)
         best 99999 ;; upper bound
         i 1]
    (let [[state cost] (peek states)]
      #_(when (= 0 (mod i 10000))
          (Thread/sleep 1)
          (println "* " best (count states) "current=" cost))

      (cond
        (nil? state)
        best

        (> cost best)
        best

        (win? state)
        (recur
          (pop states)
          cost
          (inc i))

        :else
        (let [new-states (all-moves state)]
          (recur
            (merge-with min ;; eliminate duplicate states, part1 was fine w/o this
              (pop states)
              (into {}
                (for [[next-state move-cost] new-states]
                  [next-state (+ cost move-cost)])))
            best
            (inc i)))))))


;; test some flows just to see if things make sense
;; (follow test-input first)
;; (follow test-input rand-nth)
(defn follow [state chooser]
  (Thread/sleep 1)
  (println "*" (boolean (win? state)) state)

  (when (not (win? state))
    (when-let [next-state (chooser (keys (all-moves state)))]
      (recur next-state chooser))))

;; ---------- results

(comment
  (part2 test-input)
  44169

  (part2 full-input)
  48708)
