(ns day16.main
  (:require [clojure.java.io :as io]))

;; the parser here is a mess, and the names are horrible, but
;; it's good enough for AoC....

;; ---------- input
(def hex-table
  {"0" "0000"
   "1" "0001"
   "2" "0010"
   "3" "0011"
   "4" "0100"
   "5" "0101"
   "6" "0110"
   "7" "0111"
   "8" "1000"
   "9" "1001"
   "A" "1010"
   "B" "1011"
   "C" "1100"
   "D" "1101"
   "E" "1110"
   "F" "1111"})

(defn parse-binary [bstr]
  (Integer/parseInt bstr 2))

(defn parse [text]
  (->> (seq text)
    (map str)
    (map hex-table)
    (reduce str)))

(def full-input (parse (slurp (io/resource "day16/input.txt"))))

;; ---------- part1
(defn read-number [bits nbits]
  (parse-binary (apply str (take nbits bits))))

(defn read-literal [bits]
  (loop [i 0
         acc 0]
    (let [n (read-number (.substring bits i) 5)]
      (if (>= n 16)
        (recur
          (+ i 5)
          (+ (* acc 16) (- n 16)))
        [(+ i 5) (+ (* acc 16) n)]))))


(declare read-all)
(declare read-n)

(defn read-subpackets [bits]
  (if (= 1 (read-number bits 1))
    (let [npackets (read-number (.substring bits 1) 11)
          [read subpackets] (read-n (.substring bits 12) npackets)]
      [(+ 12 read) subpackets])
    (let [nbytes (read-number (.substring bits 1) 15)]
      [(+ 16 nbytes) (read-all (.substring bits 16 (+ 16 nbytes)))])))

(defn read-one [bits]
  (let [header
        {:version (read-number bits 3)
         :type (read-number (.substring bits 3) 3)
         :length 6}]
    (cond
      (= 4 (:type header))
      (let [[read literal] (read-literal (.substring bits (:length header)))]
        (-> header
          (assoc :literal literal)
          (update :length (partial + read))))

      :else ;; operator
      (let [[read packets]
            (read-subpackets (.substring bits (:length header)))]
        (-> header
          (update :length (partial + read ))
          (assoc :subpackets packets))))))

(defn read-n [bits howmany]
  (if (= 1 howmany)
    (let [packet (read-one bits)]
      [(:length packet) [packet]])
    (let [packet (read-one bits)
          [read packets] (read-n (.substring bits (:length packet)) (dec howmany))]
      [(+ read (:length packet))
       (concat [packet] packets)])))

(defn read-all [bits]
  (loop [bits bits
         packets []]
    (if (zero? (count bits))
      packets
      (let [new-packet (read-one bits)]
        (recur
          (.substring bits (:length new-packet))
          (conj packets new-packet))))))

(defn add-versions [packet]
  (apply + (:version packet) (map add-versions (:subpackets packet))))

(defn part1 [input]
  (add-versions (read-one input)))

;; -------------------- part 2

(defn binary-truth [val]
  (if val 1 0))

(defn evaluate [packet]
  (cond
    (= 0 (:type packet))
    (reduce + (map evaluate (:subpackets packet)))
    (= 1 (:type packet))
    (reduce * (map evaluate (:subpackets packet)))

    (= 2 (:type packet))
    (reduce min (map evaluate (:subpackets packet)))
    (= 3 (:type packet))
    (reduce max (map evaluate (:subpackets packet)))
    (= 4 (:type packet))
    (:literal packet)
    (= 5 (:type packet))
    (binary-truth
      (>
        (evaluate (first (:subpackets packet)))
        (evaluate (second (:subpackets packet)))))

    (= 6 (:type packet))
    (binary-truth
      (<
        (evaluate (first (:subpackets packet)))
        (evaluate (second (:subpackets packet)))))

    (= 7 (:type packet))
    (binary-truth
      (=
        (evaluate (first (:subpackets packet)))
        (evaluate (second (:subpackets packet)))))

    :else
    (throw (Exception. (str "unknown packet " (:type packet))))))

(defn part2 [input]
  (evaluate (read-one input)))

;; ----------

(comment
  (part1 full-input)
  893
  (part2 full-input)
  4358595186090

  )
