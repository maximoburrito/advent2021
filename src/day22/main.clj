(ns day22.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;; ---------- input

(defn parse-int [nstr]
  (Integer/parseInt nstr))

(defn parse-range [text]
  (let [[_ from to] (re-matches #"(\-?\d+)\.\.(\-?\d+)" text)]
    [(parse-int from) (parse-int to)]))

(defn parse-line [line]
  (let [[_ on-off x y z](re-matches #"(\w+) x=(.*),y=(.*),z=(.*)" line)]
    [(if (= on-off "on") 1 0)
     (parse-range x)
     (parse-range y)
     (parse-range z)]))


(defn parse-input [text]
  (map parse-line (str/split-lines text)))

(def test1-input (parse-input (slurp (io/resource "day22/test.txt"))))
(def test2-input (parse-input (slurp (io/resource "day22/test2.txt"))))
(def full-input (parse-input (slurp (io/resource "day22/input.txt"))))

;; ---------- part2
(defn test-cube [[on-off [x1 x2] [y1 y2] [z1 z2]] [x y z]]
  (when
      (and
        (<= x1 x x2)
        (<= y1 y y2)
        (<= z1 z z2))
      on-off))

(defn test-cubes [cubes p]
  (or (last
        (for [cube cubes
              :let [state (test-cube cube p)]
              :when state]
          state))
    0))

(defn part1 [input]
  (let [size (range -50 51)]
    (reduce +
      (for [x size
           y size
           z size]
       (test-cubes input [x y z])))))

;; ---------- part2

(defn length [[start end]]
  (inc (- end start)))

(defn area [[_ xr yr zr :as state]]
  (* (length xr) (length yr) (length zr)))


(defn clip [[start1 end1] [start2 end2]]
  (when (and (> end1 start2) (> end2 start1))
    [(max start1 start2)
     (min end1 end2)]))


;; calculate overlap, we don't care about onoff
(defn overlap-cube [[_ xr yr zr :as state] [onoff xr2 yr2 zr2 :as state2]]
  (let [cxr (clip xr2 xr)
        cyr (clip yr2 yr)
        czr (clip zr2 zr)]
    (when (and cxr cyr czr)
      [onoff cxr cyr czr])))

;; for a cube and a set of cubes after, find our area and substract
;; out overlapping areas but for each overlapping area, we substract
;; out the overlapping areas after that so things don't get subtracted
;; out multiple times.  I thought maybe this would need to be
;; memoized, but there aren't that many overlaps and the amount of work
;; to be done always decreases...
(defn diff [[_ xr yr zr :as state] more-states]
  (let [overlaps
        (for [state2 more-states
              :let [overlap (overlap-cube state state2)]
              :when overlap]
          overlap)]

    #_(when (seq overlaps)
        (println "* " state "->" (count overlaps)))

    (-
      (area state)
      (reduce + 0
        (for [i (range (count overlaps))]
          (diff (nth overlaps i)
            (drop (inc i) overlaps)))))))


(defn part2 [input]
  (loop [states input
         area 0]
    (let [state (first states)]
      (cond
        (nil? state)
        area

        ;; this one is off - nothing to add
        (zero? (first state))
        (recur (rest states)
            area)

        ;; we are on, so find the area of our state (minus anything following)
        :else
        (let [my-area (diff state (rest states))]
          (recur (rest states)
            (+ area my-area)))))))
