(ns day20.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;; I wasn't sure how to handle the inifinity, but on part1 I figured
;; I'd just add some padding and strip off the bad parts at the end. I
;; assumed I'd need to do better for part2, but since i didn't know
;; exactly how part2 would be harder, I didn't want to invest the time
;; in improving it.
;;
;; This code is slow and hacky. If we weren't coming off the tedium of
;; day19, I might feel bad and want to improve this, but I'm just happy
;; to be done. Ship it.


;; ---------- inputs
(defn dotb  [c]
  (if (= c \.) 0 1))

(defn dotc [b]
  (if (= b 0) \. \#))

(defn to-bin [header]
  (mapv dotb header))


(defn dump [image]
  (dorun
    (for [line (:body image)]
      (println (apply str (map dotc line))))))

(defn parse-input [text]
  (let [lines (str/split-lines text)
        header (first lines)
        body (mapv
                to-bin
                (drop 2 lines))]
    {:width (count (first body)) ;; also height, since inputs are squares
     :header (to-bin header)
     :body body}))

(def test-input (parse-input (slurp (io/resource "day20/test.txt"))))
(def full-input (parse-input (slurp (io/resource "day20/input.txt"))))


;; ---------- part1

(defn simple-pad [image n]
  (let [width (+ n n (:width image))
        zeroline (vec (repeat width 0))
        body (vec
               (concat
                 (repeat n zeroline)
                 (map #(vec (concat (repeat n 0) % (repeat n 0)))
                   (:body image))
                 (repeat n zeroline)))]

    (-> image
      (assoc :width width)
      (assoc :body body))))

(defn unpad [image n]
  (let [width (- (:width image) n n)
        trim-line (fn  [line]
                    (->> line
                      (drop n)
                      (take width)))
        body (vec
               (->> (:body image)
                 (drop n)
                 (mapv trim-line)
                 (take width)))]

    (-> image
      (assoc :width width)
      (assoc :body body))))


(def deltas
  [[-1 -1] [ 0 -1] [ 1 -1]
   [-1  0] [ 0  0] [ 1  0]
   [-1  1] [ 0  1] [ 1  1]])

;; compute the new value of position p
(defn body-val [state p]
  (let [grid (for [d deltas]
               (mapv + p d))
        digits (map (fn [[x y]]
                      (-> (:body state)
                        (nth y)
                        (nth x)))
                 grid)
        n (Integer/parseInt (apply str digits) 2)
        out (get-in state [:header n])]
    out))

;; this is the basic function to step through the state
;; it ignores the outer square, which is wrong, but hey...
(defn step [state]
  (let [body
        (let [border #{0 (dec (:width state))}]
          (vec
            (for [y (range (:width state))]
              (vec
                (for [x (range (:width state))]
                  (if (or (border x) (border y))
                    0  ;border stays zero
                    (body-val state [x y])))))))]
    (-> state
      (assoc :body body))))


;; count all the ones.. probably should just be a flatten...
(defn count-up [state]
  (let [count-line #(reduce + %)]
    (reduce + (map count-line (:body state)))))


;; this feels like cheating. put some padding around the outside and then
;; strip off the errors...
(defn part1 [input]
  (let [pad (simple-pad input 20)
        final (step (step pad))
        unpad (unpad final 10)]
    #_(dump unpad)
    (count-up unpad)))

;; ---------- part2
(defn stepn [state n]
  #_(println "*" n)
  (if (= n 0)
    state
    (recur (step state) (dec n))))


;; even more cheating - pad more and strip more.
;; me so smartly and coding good. hires me pleez?
(defn part2 [input]
  (let [pad (simple-pad input 200)
        final (stepn pad 50)
        unpad (unpad final 120)]
    (count-up unpad)))


;;; ---------- results

(comment
  (part1 test-input)
  35
  (part1 full-input)
  5065
  (part1 test-input)
  3351
  (part2 full-input)
  14790)

