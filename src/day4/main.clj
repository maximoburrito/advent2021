(ns day4.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;;; input

(def test-input "day4/test.txt")
(def full-input "day4/input.txt")

(defn read-board-line [line]
  (mapv #(Integer/parseInt %) (str/split (str/trim line) #"\s+")))

(defn read-board [lines]
  (mapv read-board-line (rest lines)))

(defn read-input [file]
  (let [lines (str/split-lines (slurp (io/resource file)))]
    (let [called (map #(Integer/parseInt %)
                      (str/split (first lines) #","))]
      {:calls  called
       :boards (map read-board
                   (partition 6 (rest lines)))})))

;;; part1

(defn get-row [board n]
  (board n))

(defn get-col [board n]
  (vec (map #(get % n) board)))

(defn bingo-win [board calls]
  (let [lines-to-test
        (concat (map #(get-row board %) (range 5)) (map #(get-col board %) (range 5)))

        test-line
        (fn [line]
          (every? calls line))]

    test-line
    (when (some test-line lines-to-test)
      board)))


(defn score-board [board called]
  (reduce + (remove called (flatten board))))

(defn part1 [input]
  (loop [called #{}
         calls (:calls input)]
    (if (empty? calls)
      :error
      (let [this-call (first calls)
            next-called (conj called this-call)]
        (if-let [winner (first
                         (for [board (:boards input)
                               :when (bingo-win board next-called)]
                           (* this-call (score-board board next-called))))]
          winner
          (recur next-called (rest calls)))))))


;;; part3
(defn winning-turn [board calls]
  (loop [n 1]
    (if (bingo-win board (into #{} (take n calls)))
      n
      (recur (inc n)))))


(defn part2 [input]
  (second
   (apply max-key
          first
          (for [board (:boards input)
                    :let [calls (:calls input)
                          turn (winning-turn board calls)
                          called (into #{} (take turn  calls))
                          last-called (nth calls (dec turn))
                          score (score-board board called)]]
                [turn (* last-called score)]))))


;;; expected results

(comment
  (part1 (read-input test-input))
  4512
  (part1 (read-input full-input))
  22680

  (part2 (read-input test-input))
  1924
  (part2 (read-input full-input))
  16168

  )
